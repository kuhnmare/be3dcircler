# GCode CIRCLER #

be3D assignment. 
Reads .gcode file and replaces circular line segments series with G2/G3 commands.

### How to use? ###

* Start Release/Circler.exe
* Put input filename (relative to current working directory]
* Select tolerances and other parameters
* Results written to out.gcode

### Notes ###

* Crunches 17mb test file in less than 3s.
* Sample result is saved also /Release/out.gcode file.
* Needs more testing!
* The code should be split into different files later.
* Comments in input files are ignored.
* The project filestructure is used default from VS.

* For more questions contact: Marek Kuhn, kuhnm@centrum.cz