// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <deque>
#include <tuple>

#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

// Global INPUT PARAMS
char FILEPATH[] = "kelimek.gcode";
double SEGMENT_MIN_ANGLE = 165;		// 
double SEGMENT_SIZE_TOLERANCE = 20;	// segment [constant] size tolerance in %
int CIRCLE_MIN_POINTS = 4;
double CIRCLE_TOLERANCE = 0.02;		// max error of point from approx circle

// Geometry related
struct Point {
	double x, y, e;
};

struct Circle {
	Point S;
	double r;
};

double distanceBtwPoints(const Point &A, const Point &B) {
	return sqrt(pow( A.x - B.x, 2) + pow(A.y - B.y, 2));
}


double angleBtwPoints(const Point &A, const Point &B, const Point &C) {
	// Angle Beta of triangle ABC
	double a, b, c;
	a = distanceBtwPoints(B, C);
	b = distanceBtwPoints(A, C);
	c = distanceBtwPoints(A, B);
	return acos((a*a + c*c - b*b) / (2 * c*a));
}

char arcDirection(const Point &A, const Point &B, const Point &C) {
	// the direction is in the order A -> B -> C
	// Returns 'L' if it goes CCW
	//			'R' if CW
	//			else 'N'

	double phi1 = atan2(B.y - A.y, B.x - A.x);
	double phi2 = atan2(C.y - B.y, C.x - B.x);
	bool equalSign = (phi1*phi2) > 0;
	char direction = 'N';

	if (phi2 < phi1) {
		if (phi2 < phi1 - M_PI) 
			direction = 'L';
		else 
			direction = 'R';
	}
	if (phi2 > phi1) {
		if (phi2 > phi1 + M_PI) 
			direction = 'R';
		else
			direction = 'L';
	}
	
	return direction;
}

Circle fitCircle( Point &A,  Point &B,  Point &C) {
	// Returns circle contstructed of three points
 	double det = (C.x - B.x)*(B.y - A.y) - (B.x - A.x)*(C.y - B.y);
	double xs = (C.y - B.y)*(A.x*A.x + A.y*A.y) +
		(A.y - C.y)*(B.x*B.x + B.y*B.y) +
		(B.y - A.y)*(C.x*C.x + C.y*C.y);
	xs = xs / (2 * det);
	double ys = (C.x - B.x)*(A.x*A.x + A.y*A.y) +
		(A.x - C.x)*(B.x*B.x + B.y*B.y) +
		(B.x - A.x)*(C.x*C.x + C.y*C.y);
	ys = -ys / (2 * det);
	Point s = { xs,ys,0 };
	Circle c = { s, distanceBtwPoints(s,A) };
	return c;
};

// Data IO
string getFileContents(const char *fileName) {
	ifstream is(fileName);
	if (is) {
		string contents;
		is.seekg(0, ios::end);
		contents.resize(is.tellg());
		is.read(&contents[0], contents.size());
		is.close();
		return contents ;
	}
	//http://insanecoding.blogspot.cz/2011/11/how-to-read-in-file-in-c.html

	//throw(errno);
}

tuple<string *, bool, bool> getNextRelevantCmd( string * startPtr, const string * backPtr) {
	// Finds the next G1 in the array of GCODE strings
	
	// Return tuple 
		// string pointer of the CMD, 
		// bool indication if continues (can be interrupted by other GCmd)
		// bool end of file 

	string * pos = startPtr;
	int i = 0;	
	while (pos != backPtr) {
		pos++; 
		i++;

		string cmd = *pos;
		if (cmd.substr(0,3) == "G1 " && 
			cmd.find("X", 0) != string::npos  && 
			cmd.find("Y", 0) != string::npos &&
			cmd.find("Z", 0) == string::npos &&	
			cmd.find("F", 0) == string::npos 
			) {
				
			return make_tuple(pos, (i > 1 ? 0 : 1), 0);
		}
		
	}
	return make_tuple(pos, (i > 1 ? 0 : 1), 1);
};


double getValueFromGC(const string cmd, const char param) {
	// Parse value of "parameter" from GCode 
	
	size_t begin, end;

	begin = cmd.find(param, 0);
	if (begin != string::npos) {
		end = cmd.find(" ", begin);
		if (end == string::npos) {
			end = cmd.size();
		}
	}
	else
		throw "Parameter not found";

	return stod(cmd.substr(begin+1, (end - begin)));
}


// Main executive class 
 static class Circler {

	 Circle activeCircle;
	 double activeCircleError = 0; 
	 double activeCircleMSE = 0;
	 deque<Point> activePoints;

	 double maxDist = numeric_limits<double>::min();	// holds max and min 
	 double minDist = numeric_limits<double>::max();	//	Error of point to activeCircle

 public:
	 bool active = 0;					// = circle in progress
	 char activeCircleDirection = 'N';	// "CW = R {Right} or CCW = L {left}"
	 
	//Circler() {
		// //constructor
	 //};
	 
	 bool examineNewPoint(Point p) {
		 // Check the point if elligible for possible circle replacement
		 // Approximates the circle if the point fits
		 
		 // Returns true if point fullfiled the requirements 
		 // and was pushed to activePoints queue

		 // Returns false if the point couldnt be added

		 if (activePoints.size() == 0) {
			activePoints.push_back(p);
			return true;
		 }

		 if (activePoints.size() >= 1) {

			 // Check the limit values of segment size
			 double distance = distanceBtwPoints(p, activePoints.back());
			 if (distance > maxDist)
				 maxDist = distance;	
			 if (distance < minDist) {
				 minDist = distance;
			 }
			
			 // Check the segment size
			 if ( ((maxDist - minDist)/maxDist)*100 < SEGMENT_SIZE_TOLERANCE) {
				 // Dist ok
				 // Check the angle
				 if (activePoints.size() >= 2) {
					 double angle = angleBtwPoints(activePoints[activePoints.size() - 2], activePoints.back(), p);
					 double angleLimit = SEGMENT_MIN_ANGLE*M_PI/180; //param
					 
					//double angleMaxLimit = 180 * M_PI / 180;

					 if (angle >= angleLimit) {
						 if (active == 0) {
							 // No circle is active = construct new one
							 if (activePoints.size() >= CIRCLE_MIN_POINTS) {
								 activeCircle = fitCircle(activePoints.front(), activePoints[activePoints.size() / 2], p);
								 activeCircleError = 0;
								 activeCircleMSE = 0;
								 active = 1;
								 activeCircleDirection = arcDirection(activePoints.front(), activePoints[activePoints.size() / 2], p);
							 }
								 activePoints.push_back(p);
								 return true;
						 }
						 else {
							 //(active == 1)

							 double err = abs(distanceBtwPoints(activeCircle.S, p) - activeCircle.r);
							 
							 // Does the new arc go the same direction?
							 char direction = arcDirection(
								 activePoints[activePoints.size() - 2],
								 activePoints[activePoints.size() - 1],
								 p
							 );
							 if (activeCircleDirection != direction) {
								 return false;
							 }

							 // Does it fit into current Circle within tolerances?
							 if ( err <= CIRCLE_TOLERANCE) {
								 if (err > activeCircleError)
									 activeCircleError = err;
								 activePoints.push_back(p);
								 return true;
							 }
							 else {
								 // Point off the current circle
								 // Try to reposition the approximation circle
								 Circle c = fitCircle(activePoints.front(), activePoints[activePoints.size() / 3], activePoints[(activePoints.size()/3)*2]);

								 // Is there any improvement in err?
								 double errNew = abs(distanceBtwPoints(c.S, p) - c.r);
								 if (errNew > err) {
									 return false;
								 }
								 
								 // Temporarily add P to buffer
								 activePoints.push_back(p);

								 // Mean squared error for the old circle
								 double mseOld = 0; 
								 for (int i = 0; i < activePoints.size(); i++) {
									 err = distanceBtwPoints(activePoints[i], activeCircle.S) - activeCircle.r;
									 mseOld = mseOld + pow(err, 2);
								 }
								 mseOld = mseOld / activePoints.size();


								 // Max Error, Mean squared error for new circle
								 double maxErr = 0, mse = 0;
								 for (int i = 0; i < activePoints.size(); i++) {
									 err = abs(distanceBtwPoints(activePoints[i], c.S) - c.r);
									 mse = mse + pow(err, 2);
									 if (err > maxErr) {
										 maxErr = err;
									 }
								 }
								 mse = mse / activePoints.size();

								 // Remove P from buffer
								 activePoints.pop_back();

								 if (maxErr <= activeCircleError && (mse <= mseOld || mse < 0.0005)) {
									 // Replace with new better Circle
									 activeCircle = c;
									 activeCircleError = maxErr;
									 activeCircleMSE = mse;

									 activePoints.push_back(p);
									 return true;
								 }
								 else {
									 return false;
								 }
							 }
						 }
					 }
					else {
						// (angle < angleLimit)
						return false;
					}
				 }
				 else {
					 activePoints.push_back(p);
					 return true;
				 }
			 }
			 else {
				 // (maxDist - minDist) > tolerance
				maxDist = numeric_limits<double>::min();
				minDist = numeric_limits<double>::max();
				 return false;
			 }
		 }
	 }

	 string print() {
		 // Returns Gcode command of current activeCircle

		 Point a = activePoints.front(), b = activePoints.back() ;
		 string gcode = "", cmd = "";

		 if (activeCircleDirection == 'R')
			 gcode = "G2";	// CW
		 else {
			 if (activeCircleDirection == 'L') {
				 gcode = "G3";	// CCW
			 }
		 }
			 
		 if (gcode.compare("") != 0) {
			 cmd = gcode +
				 " X" + to_string(b.x) +
				 " Y" + to_string(b.y) +
				 " I" + to_string(activeCircle.S.x - a.x) +
				 " J" + to_string(activeCircle.S.y - a.y) +
				 " E" + to_string(b.e);
		 }

		 /* DEBUG
		 // Mean square error 
		 double mse = 0, err;
		 for (int i = 0; i != activePoints.size(); ++i) {
			 err = (distanceBtwPoints(activePoints[i], activeCircle.S) - activeCircle.r);
			 mse = mse + pow(err, 2);
		 }
		 mse = mse / activePoints.size();

		 cout << "Replaced " << to_string(activePoints.size())
			 << " commands with " << cmd << " MSE:" << to_string(mse) << endl;
		*/
		 return cmd;
	 }

	 void reset() {
		 activePoints.clear();
		 active = 0;
		 activeCircleDirection = 'N';
		 maxDist = numeric_limits<double>::min();
		 minDist = numeric_limits<double>::max();
	 }

 };
 



int main()
{
	cout << endl << "GCode Circle detection" << endl;
	cout << "-----------------------------------------" << endl << endl;
	
	string buf = "";
	cout << "Zadejte jmeno souboru *.gcode (default kelimek.gcode) " << endl;
	getline(cin, buf);
	if (buf.empty()) {
		cout << "Using default path " << FILEPATH << endl;
	}
	else {
		strcpy(FILEPATH, buf.c_str()); // Perform check
	}


	
	cout << "Zadejte hranicni uhel mezi segmenty (default 165)" << endl;
	getline(cin, buf);
	if (buf.empty()) {
		cout << "Using default angle " << to_string(SEGMENT_MIN_ANGLE) << endl;
	}
	else {
		SEGMENT_MIN_ANGLE = stod(buf); // Perform check
	}

	cout << "Zadejte toleranci vzdalenosti mezi body v procentech. (default 20)" << endl;
	getline(cin, buf);
	if (buf.empty()) {
		cout << "Default tolerance " << to_string(SEGMENT_SIZE_TOLERANCE) << "%" << endl;
	}
	else {
		SEGMENT_SIZE_TOLERANCE = stod(buf); // Perform check
	}

	cout << "Loadng the input file..." << endl;

	
	// File input - NEEDS OPTIMIZATION
	string line;
	vector<string> gCmdIn(0);
	int lineCount = 0;
	
	ifstream inputFile(FILEPATH);
	if (inputFile.is_open()) {
		while (getline(inputFile, line)) {

			// destroy comments and blank lines
			if (!line.empty() && line[0] != '\r' && line.substr(0, 1) != ";" && line != "") {
				gCmdIn.push_back(line);
				lineCount++;
			}
		}
	}
	else {
		cout << "Soubor nelze otevrit. Konec.";
		string dump;
		cin >> dump;
		return 0;
	}

	//Done reading file - initializations
	string *gActiveStartPtr = &(gCmdIn[0]);	
	string *gActiveStopPtr = gActiveStartPtr;
	string *gNextActivePtr;
	const string *gBackPtr = &(gCmdIn.back());
	
	vector<string> gCmdOut(0);
	
	Circler circler = Circler();


	// MAIN LOOP
	cout << "Processing gcode..." << endl;
	int n = 0; 
	int nLineCount = 0;

	while (gActiveStartPtr != gBackPtr) {
		n++;
	
		//cout << "N "  << to_string(n) << " @" << gActiveStartPtr << " ; " << *gActiveStartPtr << endl;
		
		bool isContinuous, eof;
		tie(gNextActivePtr, isContinuous, eof) = getNextRelevantCmd(gActiveStartPtr, gBackPtr);

		//cout << "Active start cmd " << *gActiveStartPtr << endl;

		// Put ommited gcode to the output and update ActiveStartPointer
		while (gActiveStartPtr != (gNextActivePtr)) {
			gCmdOut.push_back(*gActiveStartPtr);
			gActiveStartPtr++;
		}
		gActiveStartPtr = gNextActivePtr;
		gCmdOut.push_back(*gActiveStartPtr); //push that first command

		if (eof) {
			gCmdOut.push_back(*gActiveStartPtr);  // push that last command
			cout << "End of file reached." << endl;
			break;
		}
		else{
		    
			try {
				Point p = { getValueFromGC(*gActiveStartPtr, 'X'),
							getValueFromGC(*gActiveStartPtr, 'Y'),
							getValueFromGC(*gActiveStartPtr, 'E') };
				bool dump = circler.examineNewPoint(p);
			}
			catch (int e) {
				cout << "Exception - GCode read failed." << endl;
				string dump;
				cin >> dump;
				return 0;
			}


			// Look forward for the next points
			gActiveStopPtr = gActiveStartPtr;
			isContinuous = true;

			while (isContinuous == true) {
				

				tie(gNextActivePtr, isContinuous, eof) = getNextRelevantCmd(gActiveStopPtr, gBackPtr);
				
				nLineCount = gNextActivePtr - &(gCmdIn[0]);
				
				if (isContinuous == false || eof) {
					break;
				}
				else
					gActiveStopPtr = gNextActivePtr;

				Point p;
				try {
					p = { getValueFromGC(*gActiveStopPtr, 'X'),
						getValueFromGC(*gActiveStopPtr, 'Y'),
						getValueFromGC(*gActiveStopPtr, 'E') };
				}
				catch (int e) {
					cout << "Exception - GCode read failed." << endl;
					return 0;
				}


				// Check if the points are eligible for circle 
				if (!circler.examineNewPoint(p)) {
					break;
				}

			}
		}


		// Put active region to the output.
		//gCmdOut.push_back("Tohle misto bodu hahahahaha");
		if (circler.active) {
			gCmdOut.push_back(circler.print());
		}
		else {
			while (gActiveStartPtr != gActiveStopPtr) {
				gCmdOut.push_back(*(gActiveStartPtr++));
			}
		}
		circler.reset();
		
		// Move the START pointer to the next GCommand
		// + Perform EOF check
		if (gActiveStopPtr != gBackPtr)
			gActiveStartPtr = gActiveStopPtr + 1;
		else
			break;

		// Display percentage done
		if ( (n % 1000) == 0)
			cout << "Done " << to_string(100*double((gCmdIn.size() - (gBackPtr - gActiveStartPtr)))/gCmdIn.size() ) << "%" << endl;
	}


	// File output
	ofstream outFile;
	outFile.open("out.gcode");
	for (vector<string>::iterator it = gCmdOut.begin(); it != gCmdOut.end(); ++it) {
		outFile << *(it) << endl;
	}
	
	outFile.close();

	cout << "Saved. Done. " << endl;
	string dump;
	cin >> dump;

    return 0;
}

